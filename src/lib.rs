//! # Async patterns and examples
//!
//! This will contain small snippets and patterns that can be used in async code.
//! I am using this as I learn more about async so I can solidify the knowledge
//! as well as providing a cheatsheet for common cases.
//!
//! ## Disclaimer:
//! I do not claim to be a async or Rust expert so take this examples with a grain of salt.
//! If something can be improved then let me know.

#![allow(dead_code)]
#![deny(clippy::pedantic)]
#![deny(clippy::all)]

use std::fmt::Display;
use std::ops::Range;
use std::time::Duration;

use futures::{future, stream};

/// Basic function marked as async.
///
/// This isn't actually doing anything that needs to be async,
/// but this shows that you don't need to mark the internals
/// with any `Future`s when using async functions.
///
/// This is roughly :
/// `fn add(a: i32, b: i32) -> impl Future<Output = i64>`
async fn add(a: i32, b: i32) -> i64 {
    // Simulate an async task here
    tokio::time::delay_for(Duration::from_millis(1)).await;
    i64::from(a + b)
}

/// If written like syncronous code the async code
/// will run in series.
async fn double_add(a: i32, b: i32) -> i64 {
    add(a, b).await + add(a, b).await
}

/// Using `futures::join!` macro allows for multiple futures
/// to be await at the same time. This is useful for simple
/// cases where there are a couple known futures.
///
/// This does not scale though
async fn double_add_parallel(a: i32, b: i32) -> i64 {
    let (c, d) = futures::join!(add(a, b), add(a, b));
    c + d
}

/// If there are N things to do then `join_all` can be used
/// to process many things at the same time.
///
/// This is a simple API that doesn't have all the bells
/// and whistles, but it can work for basic use cases
/// of running a lot of futures at the same time.
///
/// This example is split weird just to show what happens
/// at each step
async fn sum_pairs(numbers: Vec<(i32, i32)>) -> i64 {
    // Make an iterator that contains futures
    // Calling an async function without `.await`
    // will just return the future that can be awaited later
    let list_of_futures = numbers.into_iter().map(|(a, b)| add(a, b));

    // Use `join_all` to await all items in the iterator
    let list_of_sums = future::join_all(list_of_futures).await;

    // Now we have the values so we can operate on them
    list_of_sums.into_iter().sum()
}

/// Once we get into collections of things that need to be processed asyncronously
/// then we should start using `Stream`s
///
/// A `Stream` is basically an `Iterator` that is async. Or at least it can be
/// treated that way when structuring data transformations
///
/// A common use case is to have a list of requests that need to be made and
/// then the results need to be aggregated.
///
/// This example demonstrates going through a list of urls, making a request
/// then collecting the bodies into a single `String`
async fn concat_bodies(list_of_urls: Vec<String>) -> Result<String, failure::Error> {
    // Pull in `StreamExt` and `TryStreamExt` traits
    // These will add helper methods to streams including the common iterator methods:
    // `map`, `filter`, `fold`, etc,
    //
    // These work similarly to the `Iterator` methods, but run asyncronously so
    // async blocks + `.await` can be used within them
    use stream::StreamExt;
    use stream::TryStreamExt;

    // create a stream from an iterator
    stream::iter(list_of_urls)
        // Do something async with the elements of the list
        // In this case we will make a request for each url given
        .map(|url| async move { reqwest::get(&url).await?.text().await })
        // Limit concurrent requests to 5
        // `buffer_unordered` can be used if order doesn't matter
        .buffered(5)
        // try_*** methods will exit early on error
        // this will collect into a String, unless it hits an error then it will resolve early
        .try_collect()
        // Start the stream processing
        .await
        // We now either have a Ok(String) or Err(reqwest::Error) so convert the error
        .map_err(|error| failure::err_msg(error.to_string()))
}

/// A struct to represent some status of a webservice
#[derive(serde::Serialize, serde::Deserialize, Debug, Clone)]
pub struct Status {
    result: bool,
    message: String,
}

impl Status {
    /// Create a 'failed' status
    #[must_use]
    pub fn failure<T: Display>(error: T) -> Self {
        Self {
            result: false,
            message: error.to_string(),
        }
    }
    /// Create a 'success' status
    #[must_use]
    pub fn success(message: String) -> Self {
        Self {
            result: true,
            message,
        }
    }
}

/// Since the `Status` will come from a text body
/// which has the chance of failing we create an easy
/// conversion method
/// `Result<String, Error>` -> `Status`
impl<E: Display> From<Result<String, E>> for Status {
    fn from(result: Result<String, E>) -> Self {
        match result {
            Ok(text) if text.contains("success") => Self::success(text),
            Ok(text) => Self::failure(text),
            Err(error) => Self::failure(error),
        }
    }
}

/// `String` -> `Status`
impl From<String> for Status {
    fn from(text: String) -> Self {
        if text.contains("success") {
            Self::success(text)
        } else {
            Self::failure(text)
        }
    }
}

/// Returns the body as text after making a request to the provided `url`
/// The `url` can be either a `&str` or `String`
async fn get_status<T: std::borrow::Borrow<String>>(url: T) -> Result<String, reqwest::Error> {
    reqwest::get(url.borrow()).await?.text().await
}

/// Processes a list of urls as as a stream of requests
/// Then it converts the responses into the appropriate status
async fn get_list_of_statuses(list_of_urls: &[String]) -> Vec<Status> {
    use stream::StreamExt;

    // Convert iterator into `Stream`
    stream::iter(list_of_urls)
        // Map the url to a async request
        .map(get_status)
        // Limit concurrency to 5 at a time (order doesn't matter)
        .buffer_unordered(5)
        // `then` allows us to work with the result
        // even if the conversion is sync it needs be be
        // an async function since this will be happening in
        // an async context. Or a `future::ready` can be used
        // to make a sync function work in an async context.
        .then(|status| future::ready(status.into()))
        // collect into `Vec<Status>`, the function signature
        // tells collect what to collect into
        .collect()
        .await
}

/// Using `FuturesUnordered` to send a list of requests concurrently
/// This allows the runtime to deal with concurrency however it wants
/// This would max out the potential for concurrency without having
/// to specify a buffering amount like with `map` + `buffered`
///
/// This should be used when max concurrency is needed and there are no
/// external restrictions to worry about (the service being sent the requests
/// can handle the concurrency on the other end)
async fn get_list_of_statuses_with_futures_unordered(list_of_urls: &[String]) -> Vec<Status> {
    use futures::stream::FuturesUnordered;
    use futures::stream::StreamExt;

    list_of_urls
        // turn the slice into an `Iterator``
        .iter()
        // map the url to a future that will send a request
        .map(get_status)
        // Collect this iterator into a special collection called `FuturesUnordered`
        // This type acts like a stream, but all futures are processed concurrently
        // with no concern of order
        .collect::<FuturesUnordered<_>>()
        // Now we have a stream to work with so we will use `then` to change the
        // resolved requests into a `Status` type
        // This is in the async context now so `future::ready` and similar are needed
        .then(|status| future::ready(status.into()))
        // Now call `collect` on the stream to convert it into a concrete `Vec<>`
        .collect()
        // Await will trigger the entire future chain to start resolving
        .await
}

/// This shows the interaction between an `Iterator` a `FuturesUnorderd`
/// and the `TryStreamExt` methods like `try_collect`
///
/// We start with a slice of a Vec/Array. Turn that into an `Iterator`
/// that then `map`s to a `Future`. That is then collected into the stream
/// `FuturesUnordered`. That stream is `then`ed to convert the inner type into
/// another (specifically `String` -> `Status`) while keeping the error.
/// `try_collect` is called on the stream to collect it into a `Vec<Status>` or
/// an error. This is a return early function so any unresolved futures will be cancelled.
/// Finally we have to convert the error returned by the stream into the error we want to
/// return to the caller.
async fn try_to_get_list_of_statuses(
    list_of_urls: &[String],
) -> Result<Vec<Status>, failure::Error> {
    use futures::stream::FuturesUnordered;
    use futures::stream::StreamExt;
    use futures::stream::TryStreamExt;

    list_of_urls
        // turn the slice into an `Iterator`
        .iter()
        // map the url to a future that will send a request
        .map(get_status)
        // Collect this iterator into a special collection called `FuturesUnordered`
        // This type acts like a stream, but all futures are processed concurrently
        // with no concern of order
        .collect::<FuturesUnordered<_>>()
        // Now we have a stream to work with so we will use `then` to change the
        // resolved requests from `Result<String, Error>` to `Result<Status, Error>`.
        // This is in the async context now so `future::ready` and similar are needed
        .then(|status_result| future::ready(status_result.map(Status::from)))
        // Now call `try_collect` on the stream to try and convert it into a concrete `Vec<>`
        // or return early with an error if any `Err` variant exists
        .try_collect()
        // Await will trigger the entire future chain to start resolving
        .await
        // Now map the overall error to the one we want to return
        .map_err(|error| failure::err_msg(error.to_string()))
}

/// async function that resolves after `delay`
async fn process_num_with_delay(n: i32, delay: std::time::Duration) -> i32 {
    tokio::time::delay_for(delay).await;
    n + 1
}

/// To manually add a timeout to a future a timeout `Future` needs to be used
/// `tokio` and `async-std` provide their own. So use the one for the runtime
/// that will be used.
async fn process_numbers_with_overall_timeout(
    numbers_to_process: Range<i32>,
    delay_per_process: Duration,
    overall_timeout_duration: Duration,
) -> Result<Vec<i32>, tokio::time::Elapsed> {
    tokio::time::timeout(
        overall_timeout_duration,
        process_numbers(numbers_to_process, delay_per_process),
    )
    .await
}

/// Shows wrapping an entire stream in a timeout
/// The wrapped stream has timeouts on each item being processed
///
/// This is a little confusing with the return type but if we break it down
/// it is easy to see what each part means:
///
/// Result<Vec, _> - Either the stream processed or it timed out
/// Vec<Result<_, _>> - A list that represents the processed items. The elements can either be the item or the timeout error
///
/// Because the overall timeout will drop the processed items (and their timeout errors)
/// it might be difficult to see which item might be causing the global timeout. This example
/// is mostly to show that timeouts can be nested if this is a needed functionality
async fn process_numbers_with_overall_and_individual_timeout(
    numbers_to_process: Range<i32>,
    delay_per_process: Duration,
    individual_timeout_duration: Duration,
    overall_timeout_duration: Duration,
) -> Result<Vec<Result<i32, tokio::time::Elapsed>>, tokio::time::Elapsed> {
    tokio::time::timeout(
        overall_timeout_duration,
        process_numbers_with_individual_timeout(
            numbers_to_process,
            delay_per_process,
            individual_timeout_duration,
        ),
    )
    .await
}

/// Basic stream pipeline that runs an async function on each item of the stream
async fn process_numbers(numbers_to_process: Range<i32>, delay_per_process: Duration) -> Vec<i32> {
    use stream::StreamExt;

    stream::iter(numbers_to_process)
        .map(|number| async move { process_num_with_delay(number, delay_per_process).await })
        .buffered(5)
        .collect()
        .await
}

/// Stream pipeline where the async function run on each item is wrapped in a timeout
async fn process_numbers_with_individual_timeout(
    numbers_to_process: Range<i32>,
    delay_per_process: Duration,
    timeout_duration: Duration,
) -> Vec<Result<i32, tokio::time::Elapsed>> {
    use stream::StreamExt;

    stream::iter(numbers_to_process)
        .map(|number| {
            async move {
                // Add a timeout to any async call by wrapping in a timeout future
                // Since this happens in a `map` this will be per item
                tokio::time::timeout(
                    timeout_duration,
                    process_num_with_delay(number, delay_per_process),
                )
                .await
            }
        })
        .buffered(5)
        .collect()
        .await
}

/// Stream pipeline where the async function run on each item is wrapped in a timeout
/// This will exit early on individual timeout
async fn try_process_numbers_with_individual_timeout(
    numbers_to_process: Range<i32>,
    delay_per_process: Duration,
    timeout_duration: Duration,
) -> Result<Vec<i32>, tokio::time::Elapsed> {
    use stream::StreamExt;
    use stream::TryStreamExt;

    stream::iter(numbers_to_process)
        .map(|number| {
            async move {
                tokio::time::timeout(
                    timeout_duration,
                    process_num_with_delay(number, delay_per_process),
                )
                .await
            }
        })
        .buffered(5)
        // Using try_collect will force an individual timeout error to
        // return an overall error immediately
        .try_collect()
        .await
}

#[cfg(test)]
mod tests {
    use super::*;

    type TestResult = std::result::Result<(), failure::Error>;

    #[tokio::test]
    async fn it_adds() {
        let sum = add(2, 2).await;

        assert_eq!(sum, 4);
    }

    #[tokio::test]
    async fn it_adds_again() {
        let sum = double_add(2, 2).await;

        assert_eq!(sum, 8);
    }

    #[tokio::test]
    async fn it_adds_again_in_parallel() {
        let sum = double_add_parallel(2, 2).await;

        assert_eq!(sum, 8);
    }

    #[tokio::test]
    async fn it_sums_a_list() {
        let numbers = vec![(1, 2), (3, 4), (5, 6), (7, 8)];
        let sum = sum_pairs(numbers).await;

        assert_eq!(sum, 36);
    }

    #[tokio::test]
    async fn it_concats_bodies() -> TestResult {
        let _mock = mockito::mock("GET", "/")
            .with_status(200)
            .with_body("hi")
            .create();

        let urls = vec![
            mockito::server_url(),
            mockito::server_url(),
            mockito::server_url(),
            mockito::server_url(),
            mockito::server_url(),
            mockito::server_url(),
            mockito::server_url(),
        ];

        let concated_bodies = concat_bodies(urls).await?;

        assert_eq!(concated_bodies, "hihihihihihihi");

        Ok(())
    }

    fn success_url() -> String {
        format!("{}/success", mockito::server_url())
    }

    fn failure_url() -> String {
        format!("{}/fail", mockito::server_url())
    }

    fn error_url() -> String {
        format!("{}/error", mockito::server_url())
    }

    fn wrong_url() -> String {
        "2.3.5.7".into()
    }

    fn mock_success() -> mockito::Mock {
        mockito::mock("GET", "/success")
            .with_status(200)
            .with_body("success: All good!")
            .create()
    }

    fn mock_failure() -> mockito::Mock {
        mockito::mock("GET", "/fail")
            .with_status(200)
            .with_body("This failed for a reason")
            .create()
    }

    fn mock_error() -> mockito::Mock {
        mockito::mock("GET", "/error")
            .with_status(500)
            .with_body("error")
            .create()
    }

    #[tokio::test]
    async fn it_gets_a_list_of_statuses() -> TestResult {
        let _mock_success = mock_success();
        let _mock_fail = mock_failure();
        let _mock_error = mock_error();

        let urls = vec![
            success_url(),
            success_url(),
            success_url(),
            failure_url(),
            failure_url(),
            failure_url(),
            wrong_url(),
            error_url(),
        ];

        let list_of_statuses = get_list_of_statuses(&urls).await;

        assert!(list_of_statuses.iter().all(|status| {
            dbg!(&status);
            if status.result {
                status.message.contains("success")
            } else {
                status.message.contains("fail") || status.message.contains("error")
            }
        }));

        Ok(())
    }

    #[tokio::test]
    async fn it_gets_a_list_of_statuses_with_futures_unordered() -> TestResult {
        let _mock_success = mock_success();
        let _mock_fail = mock_failure();
        let _mock_error = mock_error();

        let urls = vec![
            success_url(),
            success_url(),
            success_url(),
            failure_url(),
            failure_url(),
            failure_url(),
            wrong_url(),
            error_url(),
        ];

        let list_of_statuses = get_list_of_statuses_with_futures_unordered(&urls).await;

        assert!(list_of_statuses.iter().all(|status| {
            dbg!(&status);
            if status.result {
                status.message.contains("success")
            } else {
                status.message.contains("fail") || status.message.contains("error")
            }
        }));

        Ok(())
    }

    #[tokio::test]
    async fn it_tries_to_get_a_list_of_statuses_with_futures_unordered() -> TestResult {
        let _mock_success = mock_success();
        let _mock_fail = mock_failure();

        let urls = vec![
            success_url(),
            success_url(),
            success_url(),
            failure_url(),
            failure_url(),
            failure_url(),
        ];

        let list_of_statuses = try_to_get_list_of_statuses(&urls).await?;

        assert!(list_of_statuses.iter().all(|status| {
            dbg!(&status);
            if status.result {
                status.message.contains("success")
            } else {
                status.message.contains("fail")
            }
        }));

        Ok(())
    }

    #[tokio::test]
    async fn it_tries_to_get_a_list_of_statuses_and_fails() -> TestResult {
        let _mock_success = mock_success();
        let _mock_fail = mock_failure();

        let urls = vec![
            wrong_url(),
            success_url(),
            success_url(),
            success_url(),
            failure_url(),
            failure_url(),
            failure_url(),
        ];

        let list_of_statuses_result = try_to_get_list_of_statuses(&urls).await;

        assert!(list_of_statuses_result.is_err());

        Ok(())
    }

    #[tokio::test]
    async fn it_gets_a_list_of_nums_without_timing_out() -> TestResult {
        let processed_numbers = process_numbers_with_individual_timeout(
            0..10,
            Duration::from_millis(10),
            Duration::from_secs(5),
        )
        .await;

        assert!(processed_numbers.iter().all(Result::is_ok));

        Ok(())
    }

    #[tokio::test]
    async fn it_gets_a_list_of_nums_without_timing_out_barely() -> TestResult {
        let processed_numbers = process_numbers_with_individual_timeout(
            0..10,
            Duration::from_secs(1),
            Duration::from_secs(1),
        )
        .await;

        assert!(processed_numbers.iter().all(Result::is_ok));

        Ok(())
    }

    #[tokio::test]
    async fn it_gets_a_list_of_nums_but_timeout() -> TestResult {
        let processed_numbers = process_numbers_with_individual_timeout(
            0..3,
            Duration::from_secs(2),
            Duration::from_secs(1),
        )
        .await;

        assert!(processed_numbers.iter().all(Result::is_err));

        Ok(())
    }

    #[tokio::test]
    async fn it_gets_a_list_of_nums_before_overall_timeout() -> TestResult {
        let number_processing_result = process_numbers_with_overall_timeout(
            1..3,
            Duration::from_millis(10),
            Duration::from_secs(5),
        )
        .await;

        assert!(number_processing_result.is_ok());

        Ok(())
    }

    #[tokio::test]
    async fn it_times_out_before_it_gets_a_list_of_nums() -> TestResult {
        let number_processing_result = process_numbers_with_overall_timeout(
            1..10,
            Duration::from_secs(1),
            Duration::from_secs(1),
        )
        .await;

        assert!(number_processing_result.is_err());

        Ok(())
    }
}
